package com.example.shot;

import androidx.appcompat.app.AppCompatActivity;
import androidx.work.PeriodicWorkRequest;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import worker.CitationWorker;

public class MainActivity extends AppCompatActivity {

    Context cx;
    RequestQueue queue;
    TextView tvCitation, tvAuteurDate, tvFait;

    final String urlCdJ = "http://10.0.2.2:3000/cdj";
    final String urlFait = "http://10.0.2.2:3000/fait";
    final String ACCESS_TOKEN = "967d1dc7-555b-4ee3-a98c-3210245e4a11";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.cx = this;

        tvCitation =  findViewById(R.id.tvCitation);
        tvAuteurDate =  findViewById(R.id.tvAuteurDate);
        tvFait = findViewById(R.id.tvFait);
        Button btnCDJ = findViewById(R.id.btnCDJ);


        queue = Volley.newRequestQueue(this);

        btnCDJ.setOnClickListener(view -> {
            requeteCDJ();
        });
        requeteFait();

        PeriodicWorkRequest citationRequest =
                new PeriodicWorkRequest.Builder(CitationWorker.class, 15, TimeUnit.MINUTES)
                        .build();
    }



    private void requeteCDJ() {
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, urlCdJ, null,
                response -> {
                    try {
                        tvCitation.setText(response.getString("citation"));
                        tvAuteurDate.setText(getString(R.string.auteur_date, response.getString("auteur"), response.getString("annee")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    VolleyLog.e("Error: ", ((VolleyError) error).getMessage());
                }
                );
        queue.add(req);
    }
    private void requeteFait() {

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, urlFait, null,
                response -> {
                    try {
                        tvFait.setText(response.getString("description"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    VolleyLog.e("Error: ", ((VolleyError) error).getMessage());
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("shot-key-api", ACCESS_TOKEN);
                return params;
            }
        };
        queue.add(req);
    }
}